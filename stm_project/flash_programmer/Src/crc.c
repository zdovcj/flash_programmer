#include "crc.h"


uint8_t crc_calculate(uint8_t *data, size_t length, uint8_t initial, uint8_t poly)
{
    uint8_t crc = initial;
    size_t i, j;
    for (i = 0; i < length; i++) {
        crc ^= data[i];
        for (j = 0; j < 8; j++) {
            if ((crc & 0x80) != 0)
                crc = (uint8_t)((crc << 1) ^ poly);
            else
                crc <<= 1;
        }
    }
    return crc;
}

/*
uint8_t crc_calculate(uint8_t message[], uint8_t length, uint8_t initial, uint8_t polynomial) {
  uint8_t i, j, crc = initial;

  for (i = 0; i < length; i++)
  {
    crc ^= message[i];
    for (j = 0; j < 8; j++)
    {
      if (crc & 1)
        crc ^= polynomial;
      crc >>= 1;
    }
  }
  return crc;
}
*/

