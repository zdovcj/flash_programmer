#include "spi_flash.h"

static void _spi_cs_assert(void);
static void _spi_cs_deassert(void);
static uint8_t _spi_flash_read_status_register(void);
static uint32_t _spi_flash_check_busy(void);
static void _spi_flash_wait_idle(void);
static void _spi_flash_write_enable(void);

volatile HAL_StatusTypeDef spi_status;

void spi_flash_write_blocking(uint32_t address, uint32_t size, uint8_t *data) {
	uint8_t command[] = {(uint8_t)SPI_FLASH_COMMAND_PAGE_PROGRAMM, (uint8_t)((address >> 16) & 0xff), (uint8_t)((address >> 8) & 0xff), (uint8_t)((address >> 0) & 0xff)};
	
	_spi_flash_write_enable();
		
	_spi_cs_assert();
	HAL_SPI_Transmit(&hspi1, command, 4,    30);
	HAL_SPI_Transmit(&hspi1, data   , size, 30);
	_spi_cs_deassert();
		
	_spi_flash_wait_idle();
	
}

void spi_flash_read_blocking(uint32_t address, uint32_t size, uint8_t *data) {
	uint8_t command[] = {(uint8_t)SPI_FLASH_COMMAND_READ_DATA, (uint8_t)((address >> 16) & 0xff), (uint8_t)((address >> 8) & 0xff), (uint8_t)((address >> 0) & 0xff)};
	
	_spi_cs_assert();
	HAL_SPI_Transmit(&hspi1, command, 4, 30);
	HAL_SPI_Receive(&hspi1, data, size, 30);
	_spi_cs_deassert();
}

static uint8_t _spi_flash_read_status_register(void) {
	uint8_t command[] = {(uint8_t)SPI_FLASH_COMMAND_READ_DATA};
	uint8_t readback;
	
	_spi_cs_assert();
	HAL_SPI_Transmit(&hspi1, command, 1, 30);
	HAL_SPI_Receive(&hspi1, &readback, 1, 30);
	_spi_cs_deassert();
	return readback;
}

static uint32_t _spi_flash_check_busy(void) {
	uint8_t status = _spi_flash_read_status_register();
	return (status & 0x01) != 0;
}

static void _spi_flash_wait_idle(void) {
	while(_spi_flash_check_busy());
}

static void _spi_flash_write_enable(void) {
	uint8_t command[] = {(uint8_t)SPI_FLASH_COMMAND_WRITE_ENABLE};
	
	_spi_cs_assert();
	spi_status = HAL_SPI_Transmit(&hspi1, command, 1, 30);
	_spi_cs_deassert();
}

void spi_flash_reset(void) {
	uint8_t command[] = {(uint8_t)SPI_FLASH_COMMAND_RESET_ENABLE};
	
	_spi_cs_assert();
	spi_status = HAL_SPI_Transmit(&hspi1, command, 1, 30);
	_spi_cs_deassert();
	
	command[0] = (uint8_t)SPI_FLASH_COMMAND_RESET;
	_spi_cs_assert();
	spi_status = HAL_SPI_Transmit(&hspi1, command, 1, 30);
	_spi_cs_deassert();
	
}

void spi_flash_erase_sector_4k(uint32_t address) {
	uint8_t command[] = {(uint8_t)SPI_FLASH_COMMAND_SECTOR_ERASE, (uint8_t)((address >> 16) & 0xff), (uint8_t)((address >> 8) & 0xff), (uint8_t)((address >> 0) & 0xff)};
	
	_spi_flash_write_enable();
		
	_spi_cs_assert();
	HAL_SPI_Transmit(&hspi1, command, 4,    30);
	_spi_cs_deassert();
		
	_spi_flash_wait_idle();
}

static void _spi_cs_assert(void) {
	HAL_GPIO_WritePin(SPI_CS_PORT, SPI_CS_PIN, GPIO_PIN_RESET);	
}

static void _spi_cs_deassert(void) {
	HAL_GPIO_WritePin(SPI_CS_PORT, SPI_CS_PIN, GPIO_PIN_SET);	
}

