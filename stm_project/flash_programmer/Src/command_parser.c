#include "command_parser.h"

CommandParser_State_t state = COMMAND_PARSER_STATE_IDLE;
uint8_t command;
uint32_t address;
uint32_t size;
uint32_t position;
uint8_t data[1024];
uint8_t crc;

static void _parse_command(CircularFifo_t *fifo);
static void _execute_command(uint8_t command, uint8_t address, uint8_t size, uint8_t *data);


void command_parser_parse_input(CircularFifo_t *fifo) {
	static uint32_t last_packet_arrived = 0;
	
	if(fifo->items > 0) {
		last_packet_arrived = HAL_GetTick();
		_parse_command(fifo);

	} else {
		if(HAL_GetTick() - last_packet_arrived > COMMAND_PARSER_TIMEOUT) {
			circular_fifo_flush(fifo);
			command_parser_reset_state_machine();
		}
	}
}

void command_parser_reset_state_machine(void) {
	state = COMMAND_PARSER_STATE_IDLE;
}

static void _parse_command(CircularFifo_t *fifo) {
	uint8_t crc_intermediate;
	uint32_t i;
	switch(state){
		case COMMAND_PARSER_STATE_IDLE:
			command = circular_fifo_pop(fifo);
			state = COMMAND_PARSER_STATE_ADDRESS;
			break;
		case COMMAND_PARSER_STATE_ADDRESS:
				for(i = 0; i < 4; i++) {
					address = (address << 8) | circular_fifo_pop(fifo);
				}
			state = COMMAND_PARSER_STATE_SIZE;
			break;
		case COMMAND_PARSER_STATE_SIZE:
			if(fifo->items >= 4) {
				for(i = 0; i < 4; i++) {
					size = (size << 8) | circular_fifo_pop(fifo);
				}
				if(size == 0) {
					state = COMMAND_PARSER_STATE_CRC;
				}
				else {
					state = COMMAND_PARSER_STATE_DATA;
				}
			}
			break;
		case COMMAND_PARSER_STATE_DATA:
			
			if(fifo->items >= size) {
				circular_fifo_pop_n(fifo, data, size);
				state = COMMAND_PARSER_STATE_CRC;
			}
			break;
		case COMMAND_PARSER_STATE_CRC:
			crc = circular_fifo_pop(fifo);
		
			crc_intermediate = 0;
			crc_intermediate = crc_calculate((uint8_t *)&command,     1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
			crc_intermediate = crc_calculate((uint8_t *)&address + 3, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
		  crc_intermediate = crc_calculate((uint8_t *)&address + 2, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
		  crc_intermediate = crc_calculate((uint8_t *)&address + 1, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
		  crc_intermediate = crc_calculate((uint8_t *)&address + 0, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
			crc_intermediate = crc_calculate((uint8_t *)&size    + 3, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
		  crc_intermediate = crc_calculate((uint8_t *)&size    + 2, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
		  crc_intermediate = crc_calculate((uint8_t *)&size    + 1, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
		  crc_intermediate = crc_calculate((uint8_t *)&size    + 0, 1,    crc_intermediate, COMMAND_PARSER_CRC_POLY);
		  crc_intermediate = crc_calculate((uint8_t *)&data,        size, crc_intermediate, COMMAND_PARSER_CRC_POLY);
			
			if(crc_intermediate == crc) {
				_execute_command(command, address, size, data);
			}
		  state = COMMAND_PARSER_STATE_IDLE;
			break;
	}
}


static void _execute_command(uint8_t command, uint8_t address, uint8_t size, uint8_t *data) {
	switch(command) {
		case COMMAND_PARSER_COMMAND_NOP:
			break;
		case COMMAND_PARSER_COMMAND_READ:
			spi_flash_read_blocking(address, size, data);
			break;
		case COMMAND_PARSER_COMMAND_WRITE: 
			spi_flash_write_blocking(address, size, data);
			break;
		case COMMAND_PARSER_COMMAND_ERASE:
			spi_flash_erase_sector_4k(address);
			break;
		default:
			break;
	}
}
















