#include "circular_fifo.h"


void circular_fifo_init(CircularFifo_t *fifo, uint8_t *buffer, uint32_t size) {
	fifo->buffer = buffer;
	fifo->size = size;
	fifo->head = 0;
	fifo->tail = 0;
	fifo->items = 0;
	
	return;
}


void circular_fifo_push(CircularFifo_t *fifo, uint8_t c) {
	if(fifo->items < fifo->size) {
		fifo->buffer[fifo->head] = c;
		fifo->items++;
		fifo->head++;
		if(fifo->head == fifo->size) {
			fifo->head = 0;
		}
	}
	return;
}

uint8_t circular_fifo_pop(CircularFifo_t *fifo) {
	uint8_t retval = fifo->buffer[fifo->tail];
	if(fifo->items > 0) {
		fifo->items--;
		fifo->tail++;
		if(fifo->tail == fifo->size) {
			fifo->tail = 0;
		}
	}
	return retval;
}

uint32_t circular_fifo_pop_all(CircularFifo_t *fifo, uint8_t *buffer){
	int i;
	for(i = 0; fifo->items; i++) {
		buffer[i] = circular_fifo_pop(fifo);
	}
	return i;
}

void circular_fifo_flush(CircularFifo_t *fifo) {
	fifo->head = 0;
	fifo->tail = 0;
	fifo->items = 0;
	
	return;
}

void circular_fifo_pop_n(CircularFifo_t *fifo, uint8_t *buffer, uint32_t n) {
	uint32_t i;
	for(i = 0; i < n; i++) {
		buffer[i] = circular_fifo_pop(fifo);
	}
	return;
}

