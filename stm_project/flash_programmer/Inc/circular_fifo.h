#ifndef __CIRCULAR_FIFO_H
#define __CIRCULAR_FIFO_H


#include "stm32f1xx_hal.h"

typedef struct {
	uint32_t head;
	uint32_t tail;
	uint32_t size;
	uint32_t items;
	uint8_t *buffer;
} CircularFifo_t;

void circular_fifo_init(CircularFifo_t *fifo, uint8_t *buffer, uint32_t size);
void circular_fifo_push(CircularFifo_t *fifo, uint8_t c);
uint8_t circular_fifo_pop(CircularFifo_t *fifo);
uint32_t circular_fifo_pop_all(CircularFifo_t *fifo, uint8_t *buffer);
void circular_fifo_flush(CircularFifo_t *fifo);

void circular_fifo_pop_n(CircularFifo_t *fifo, uint8_t *buffer, uint32_t n);

#endif // __CIRCULAR_FIFO_H
