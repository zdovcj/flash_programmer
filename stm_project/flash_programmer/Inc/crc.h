#ifndef __CRC_H
#define __CRC_H

#include "stm32f1xx_hal.h"

uint8_t crc_calculate(uint8_t *data, size_t length, uint8_t initial, uint8_t poly);
//uint8_t crc_calculate(uint8_t message[], uint8_t length, uint8_t initial, uint8_t polynomial);

#endif //__CRC_H
