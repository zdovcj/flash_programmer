#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H

#include "stm32f1xx_hal.h"
#include "spi.h"

#define SPI_FLASH_COMMAND_WRITE_ENABLE    0x06
#define SPI_FLASH_COMMAND_READ_DATA       0x03
#define SPI_FLASH_COMMAND_PAGE_PROGRAMM   0x02
#define SPI_FLASH_COMMAND_SECTOR_ERASE    0x20
#define SPI_FLASH_COMMAND_BLOCK_ERASE_32K 0x52
#define SPI_FLASH_COMMAND_BLOCK_ERASE_64K 0xD8
#define SPI_FLASH_COMMAND_CHIP_ERASE      0xC7
#define SPI_FLASH_COMMAND_READ_SR         0x05
#define SPI_FLASH_COMMAND_RESET_ENABLE    0x66
#define SPI_FLASH_COMMAND_RESET           0x99


#define SPI_CS_PIN GPIO_PIN_4
#define SPI_CS_PORT GPIOA


void spi_flash_write_blocking(uint32_t address, uint32_t size, uint8_t *data);
void spi_flash_read_blocking(uint32_t address, uint32_t size, uint8_t *data);
void spi_flash_reset(void);
void spi_flash_erase_sector_4k(uint32_t address);

#endif // __SPI_FLASH_H
