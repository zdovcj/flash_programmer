#ifndef __COMMAND_PARSER_H
#define __COMMAND_PARSER_H

#include "stm32f1xx_hal.h"
#include "circular_fifo.h"
#include "crc.h"

#include "spi_flash.h"

#define COMMAND_PARSER_CRC_POLY 0xe7

#define COMMAND_PARSER_TIMEOUT 100

#define COMMAND_PARSER_COMMAND_NOP 	 0x00
#define COMMAND_PARSER_COMMAND_READ	 0x01
#define COMMAND_PARSER_COMMAND_WRITE 0x02
#define COMMAND_PARSER_COMMAND_ERASE 0x03

typedef enum{
	COMMAND_PARSER_STATE_IDLE = 0,
	COMMAND_PARSER_STATE_ADDRESS = 1,
	COMMAND_PARSER_STATE_SIZE = 2,
	COMMAND_PARSER_STATE_DATA = 3,
	COMMAND_PARSER_STATE_CRC = 4
} CommandParser_State_t;



void command_parser_init(void);
void command_parser_parse_input(CircularFifo_t *fifo);
void command_parser_reset_state_machine(void);

void execute_command(uint8_t command, uint8_t address, uint8_t size, uint8_t *data);
#endif // __COMMAND_PARSER_H
