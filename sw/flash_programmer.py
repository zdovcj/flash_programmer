import serial
import crcmod

crc_function = crcmod.mkCrcFun(0x1e7, initCrc = 0, rev=False)

command = [0x01, 0xab, 0xbc, 0xcd, 0xde, 0x00, 0x00, 0x00, 0x02, 0xab, 0xcd]

data_to_send = command + [crc_function(bytes(command))]

socket = serial.Serial("COM43", 115200)

socket.write(data_to_send)